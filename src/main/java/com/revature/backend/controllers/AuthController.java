package com.revature.backend.controllers;

import com.revature.backend.models.LoginRequest;
import com.revature.backend.models.User;
import com.revature.backend.models.UserDTO;
import com.revature.backend.repositories.UserRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "http://localhost:4200", allowCredentials = "true")
public class AuthController {

    private final UserRepository userRepository;

    public AuthController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping("/login")
    public ResponseEntity<UserDTO> login(@RequestBody LoginRequest loginRequest, HttpSession session) {
        Optional<User> user = this.userRepository.findByUsernameAndPassword(
                                                    loginRequest.getUsername(),
                                                    loginRequest.getPassword());
        if(!user.isPresent()) {
            // If the user is not present, this means the request had an incorrect username or password
            return ResponseEntity.badRequest().build();
        }

        session.setAttribute("user", user.get());

        return ResponseEntity.ok(new UserDTO(user.get()));
    }

    @PostMapping("/logout")
    public ResponseEntity<Void> logout(HttpSession session) {
        session.removeAttribute("user");
        return ResponseEntity.ok().build();
    }
}
